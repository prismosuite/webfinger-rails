# Webfinger::Rails
Short description and motivation.

## Usage
How to use my plugin.

## Installation
Add this line to your application's Gemfile:

```ruby
gem 'webfinger-rails'
```

And then execute:
```bash
$ bundle
```

Or install it yourself as:
```bash
$ gem install webfinger-rails
```

## Contributing
Contribution directions go here.

## License
The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
