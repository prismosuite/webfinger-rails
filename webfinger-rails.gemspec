$:.push File.expand_path("lib", __dir__)

# Maintain your gem's version:
require "webfinger/rails/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "webfinger-rails"
  s.version     = Webfinger::Rails::VERSION
  s.authors     = ["mbajur"]
  s.email       = ["mbajur@gmail.com"]
  s.homepage    = "https://gitlab.com/mbajur"
  s.summary     = "Webfinger engine for Rails"
  s.description = "Webfinger engine for Rails"
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  s.add_dependency "rails", "~> 5.2.0"

  s.add_development_dependency "sqlite3"
end
