module Webfinger
  module Rails
    class WebfingerController < Webfinger::Rails::ApplicationController
      before_action { response.headers['Vary'] = 'Accept' }

      def show
        @account = find_account_from_resource

        respond_to do |format|
          format.any(:json, :html) { render_response }
        end

      rescue ActiveRecord::RecordNotFound
        head 404
      end

      private

      def find_account_from_resource
        raise NotImplementedError
      end

      def render_response
        raise NotImplementedError
      end

      def resource_param
        params.require(:resource)
      end
    end
  end
end
